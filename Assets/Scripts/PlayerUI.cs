﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerUI : MonoBehaviour 
{
	[SerializeField]
    RectTransform _fuelAmountFill;
    private PlayerManager _player;
    private EntityController _controller;
    [SerializeField]
    GameObject _pauseMenu;
    [SerializeField]
    GameObject _scoreBoard;
    [SerializeField]
    Text _ammoText;
    [SerializeField]
    HealthBar _healthBar;
    private WeaponManager _weaponManager;
    public void Start()
    {
        PauseMenu.IsOn = false;
    }

    public void SetPlayer(PlayerManager player)
    {
        _player = player;
        _controller = _player.GetComponent<EntityController>();
        _weaponManager = _player.GetComponent<WeaponManager>();
        _healthBar.SetPlayer(player);
    }

    public void SetController(EntityController c)
    {
        _controller = c;
    }
    void SetFuelAmount(float amount)
    {
        _fuelAmountFill.localScale = new Vector3(1f, amount, 1f);
    }

    
    void SetAmmoAmount(int amount)
    {
        _ammoText.text = amount.ToString();
    }

    void Update()
    {
        if (_controller!=null)
            SetFuelAmount(_controller.GetThrusterFuelAmount());
        
        /*if (_player != null && _healthBarFill!=null)
        {
            SetHealthAmount(_player.GetHealthPercentage() );
        }*/
        if (_weaponManager != null && _ammoText != null)
        {
            SetAmmoAmount(_weaponManager.GetCurrentWeapon().bullets);
        }

        if (Input.GetKeyDown(KeyCode.Escape) )
        {
            TogglePauseMenu();
        }

        if (Input.GetKeyDown(KeyCode.Tab) )
        {
            _scoreBoard.SetActive(true);
        }
        else if (Input.GetKeyUp(KeyCode.Tab) )
        {
            _scoreBoard.SetActive(false);
        }
    }

    public void TogglePauseMenu()
    {
        PauseMenu.IsOn = !_pauseMenu.activeSelf;
        _pauseMenu.SetActive(PauseMenu.IsOn);
    }


}

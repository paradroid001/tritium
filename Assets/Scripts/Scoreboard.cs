﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scoreboard : MonoBehaviour 
{
    [SerializeField]
    private GameObject _scoreboardItemPrefab;
    [SerializeField]
    Transform _playerScoreboardList;

    void OnEnable()
    {
        //get an array of players
        PlayerManager[] players = NetworkGameManager.GetAllPlayers();

        //loop through and set up a list item for each player
        // - setting ui elems equal to data
        foreach (PlayerManager player in players)
        {
            //Debug.Log(player.playerInfo.playerName + "|" + player.kills + "|" + player.deaths);
            GameObject go = (GameObject)Instantiate(_scoreboardItemPrefab, _playerScoreboardList);
            ScoreBoardItem item = GetComponent<ScoreBoardItem>();
            if (item != null)
            {
                item.Setup(player.username, player.kills, player.deaths);
            }

        }
    }

    void OnDisable()
    {
        //clean up our list of items
        foreach (Transform child in _playerScoreboardList)
        {
            Destroy(child.gameObject);
        }
    }
}

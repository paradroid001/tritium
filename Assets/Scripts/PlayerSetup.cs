﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(PlayerManager))]
[RequireComponent(typeof(EntityController))]
public class PlayerSetup : NetworkBehaviour 
{
    [SerializeField]
    private Behaviour[] _componentsToDisable;
    [SerializeField]
    private string _remoteLayerName = "RemotePlayer";
    [SerializeField]
    private string _localDontDrawLayerName = "DontDraw";
    [SerializeField]
    private GameObject _playerGraphics;
    [SerializeField]
    GameObject _playerUIPrefab;
    [HideInInspector]
    public GameObject _playerUIInstance;
    [SerializeField]
    HealthBarPanel[] _healthBarPanels;
    [SerializeField]
    PlayerNamePlate _namePlate;

    void Start()
    {
        if (!isLocalPlayer)
        {
            DisableComponents();
            AssignRemoteLayer();
            _namePlate.SetPlayer(GetComponent<PlayerManager>() );
        }
        else
        {
            CreateLocalPlayerUI();
            // Disable player graphics for local player.
            SetLayerRecursively(_playerGraphics, LayerMask.NameToLayer(_localDontDrawLayerName) );
            GetComponent<PlayerManager>().SetupPlayer(); //initialise the player

            string un = transform.name;
            CmdSetUserName(transform.name, un);
        }
    }

    [Command]
    void CmdSetUserName(string playerId, string username)
    {
        PlayerManager player = NetworkGameManager.GetPlayer(playerId);
        if (player != null)
        {
            Debug.Log(username + " has joined");
            player.username = username;
            player.playerInfo.playerName = username;
        }
    }

    //override network behaviour method.
    public override void OnStartClient()
    {
        base.OnStartClient();
        NetworkGameManager.RegisterPlayer(GetComponent<NetworkIdentity>().netId.ToString(), GetComponent<PlayerManager>() );
    }
    
    //called when obj is destroyed, or disconnects
    void OnDisable()
    {
        Debug.Log("On disable!");
        Destroy(_playerUIInstance);
        if (isLocalPlayer)
        {
            Debug.Log("Enabling scene cam");
            NetworkGameManager.instance.EnableSceneCamera(true);
            NetworkGameManager.UnregisterPlayer(transform.name); //our transform name is our id.
        }
        else
        {
            Debug.Log("Not a local player?");
        }
    }

    void DisableComponents()
    {
        for (int i = 0; i < _componentsToDisable.Length; i++)
        {
            _componentsToDisable[i].enabled = false;
        }
    }

    void AssignRemoteLayer()
    {
        gameObject.layer = LayerMask.NameToLayer(_remoteLayerName);
    }

    void SetLayerRecursively(GameObject obj, int newlayer)
    {
        obj.layer = newlayer;

        foreach (Transform child in obj.transform)
        {
            SetLayerRecursively(child.gameObject, newlayer); 
        }
    }

    void CreateLocalPlayerUI()
    {
        _playerUIInstance = Instantiate(_playerUIPrefab);
        _playerUIInstance.name = _playerUIPrefab.name;

        //configure player ui
        PlayerUI ui = _playerUIInstance.GetComponent<PlayerUI>();
        if (ui == null)
            Debug.LogError("No PlayerUI component on PlayerUI prefab!");
        else
        {
            //give the player controller to the player ui.
            //ui.SetController(GetComponent<EntityController>());
            ui.SetPlayer(GetComponent<PlayerManager>() );

            _healthBarPanels = GameObject.FindObjectsOfType<HealthBarPanel>();
            /*
            foreach (HealthBarPanel hbp in _healthBarPanels)
            {
                if (hbp.gameObject.layer == LayerMask.NameToLayer("LocalPlayer") )
                {
                    hbp.CreateHealthBar(GetComponent<PlayerManager>() );
                }
            }*/
        }

        Debug.Log("Created Player UI");
    }
}

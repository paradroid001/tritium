﻿public class PlayerInfo
{
    public string playerName = "Unknown";
    public int kills = 0;
    public int assists = 0;
    public int deaths = 0;
    public float damage = 0.0f;
    public float blocked = 0.0f;
    public float healed = 0.0f;
    public float maxHealth = 0.0f;
    public float currentHealth = 0.0f;
}

﻿using System.Collections;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(PlayerSetup))]
public class PlayerManager : NetworkBehaviour 
{
    [SerializeField]
    private float _maxHealth = 100;
    [SyncVar] //we want current health synced to all clients
    private float _currentHealth;
    [SerializeField]
    private GameObject _deathEffect;
    [SerializeField]
    private GameObject _spawnEffect;
    
    [SyncVar]
    public string username;
    public int kills;
    public int deaths;

    [SyncVar]
    public PlayerInfo playerInfo;

    [SyncVar]
    private bool _isDead = false;
    public bool isDead
    {
        get {return _isDead;}
        protected set {_isDead = value;}
    }
    
    [SerializeField]
    private Behaviour[] disableOnDeath;
    [SerializeField]
    private GameObject[] disableGameObjectsOnDeath;
    private bool[] wasEnabled;
    private bool _firstSetup = true;

    public void Awake()
    {
        playerInfo = new PlayerInfo();
    }

    //This only gets called on the client of a local player.
	public void SetupPlayer () 
    {
        if (isLocalPlayer)
        {
            //Switch Cams
            NetworkGameManager.instance.EnableSceneCamera(false);
            GetComponent<PlayerSetup>()._playerUIInstance.SetActive(true);
        }
                
        //Tell the server to set up the player
        CmdBroadcastPlayerSetup();
    }

    public float GetHealthPercentage()
    {
        return (float)_currentHealth / _maxHealth;
    }


    [Command]
    private void CmdBroadcastPlayerSetup()
    {
        RpcSetupPlayerOnAllClients();
    }
	
    [ClientRpc]
    private void RpcSetupPlayerOnAllClients()
    {
        if (_firstSetup)
        {
            //Save the enabled state of all the components
            wasEnabled = new bool[disableOnDeath.Length];
            for (int i = 0; i < wasEnabled.Length; i++)
            {
                wasEnabled[i] = disableOnDeath[i].enabled;
            }
            _firstSetup = false;
        }
        ResetPlayer();
    }

	// Update is called once per frame
	void Update () 
    {
	    if (isLocalPlayer)
        {
            if (Input.GetKeyDown(KeyCode.K))
            {
                RpcTakeDamage(10000, transform.name);
            }
        }
	}

    public void ResetPlayer()
    {
        isDead = false;
	    _currentHealth = _maxHealth;
        //enable components
        for (int i = 0; i < disableOnDeath.Length; i++)
        {
            disableOnDeath[i].enabled = wasEnabled[i];
        }
        //enable gameobjects
        for (int i = 0; i < disableGameObjectsOnDeath.Length; i++)
        {
            disableGameObjectsOnDeath[i].SetActive(true);
        }

        EnableCollider(true);
        
        //show the spawn effect
        GameObject seffect = (GameObject)Instantiate(_spawnEffect, transform.position, Quaternion.identity);
        //Destroy the explosion after 3 seconds
        Destroy(seffect, 3.0f);

    }

    [ClientRpc] //Called on all clients.
    public void RpcTakeDamage(int amount, string sourceID)
    {
        if (isDead)
            return;

        _currentHealth -= amount;
        Debug.Log(transform.name + " now has " + _currentHealth + " health");
        if (_currentHealth <= 0.0f)
        {
            Die(sourceID);
        }
    }

    public void Die(string sourceID)
    {
        isDead = true;
        PlayerManager sourcePlayer = NetworkGameManager.GetPlayer(sourceID);
        if (sourcePlayer != null)
        {
            //Increase that player's kill count
            sourcePlayer.kills++;
            NetworkGameManager.instance.onPlayerKilledCallback.Invoke(username, sourcePlayer.username);
        }
        deaths++;


        //DISABLE COMPONENTS
        for (int i = 0; i < disableOnDeath.Length; i++)
        {
            disableOnDeath[i].enabled = false;
        }
        EnableCollider(false);
        for (int i = 0; i < disableGameObjectsOnDeath.Length; i++)
        {
            disableGameObjectsOnDeath[i].SetActive(false);
        }


        //Spawn an explosion on each client.
        GameObject explosion = (GameObject)Instantiate(_deathEffect, transform.position, Quaternion.identity);
        //Destroy the explosion after 3 seconds
        Destroy(explosion, 3.0f);

        if (isLocalPlayer)
        {
            NetworkGameManager.instance.EnableSceneCamera(true);
            GetComponent<PlayerSetup>()._playerUIInstance.SetActive(false);
        }

        Debug.Log(transform.name + " is DEAD");
        //CALL RESPAWN METHOD after some seconds
        StartCoroutine(Respawn(NetworkGameManager.instance.matchSettings.respawnTime) );
    }

    private IEnumerator Respawn(float seconds)
    {
        yield return new WaitForSeconds(seconds);
        Transform spawnpoint = NetworkManager.singleton.GetStartPosition();
        transform.position = spawnpoint.position;
        transform.rotation = spawnpoint.rotation;
        Debug.Log(transform.name + " respawned");
        
        //give time for this pos update to be on all clients.
        //I dont like this ugly hack.
        //Would a better way be to pass a pos and rot to the setupplayer method?
        yield return new WaitForSeconds(0.1f);

        SetupPlayer();
    }

    private void EnableCollider(bool val)
    {
        Collider col = GetComponent<Collider>();
        if (col != null)
        {
            col.enabled = val;
        }

    }

}

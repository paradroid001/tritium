﻿using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;
using UnityEngine.UI;

public class RoomListItem : MonoBehaviour 
{
    private MatchInfoSnapshot _matchInfo;
    [SerializeField]
    Text roomNameText;
    public delegate void JoinRoomDelegate(MatchInfoSnapshot match);
    private JoinRoomDelegate _joinRoomDelegate;

    public void Setup(MatchInfoSnapshot mi, JoinRoomDelegate jrd)
    {
        _matchInfo = mi;
        _joinRoomDelegate = jrd;
        roomNameText.text = mi.name + "(" + mi.currentSize + "/" + mi.maxSize + ")";
    }

    public void JoinGame()
    {
        _joinRoomDelegate(_matchInfo);
    }
}

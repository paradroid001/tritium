﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;

public class Health : NetworkBehaviour 
{
    public const int maxHealth = 100;
    [SyncVar (hook = "OnChangeHealth")]
    public int currentHealth = maxHealth;
    public RectTransform healthBar;
    public bool destroyOnDeath = false;
    private NetworkStartPosition[] spawnPoints;

    public void Start()
    {
        if (isLocalPlayer)
        {
            spawnPoints = FindObjectsOfType<NetworkStartPosition>();
        }
    }

    public void TakeDamage(int damage)
    {
        if (!isServer)
            return;

        /*** This is all server code ***/
        Debug.Log("Took damage: " + damage);
        currentHealth -= damage;
        if (currentHealth <= 0)
        {

            if (destroyOnDeath)
            {
                Destroy(gameObject);
            }
            else
            {
                Debug.Log("Dead!");
                currentHealth = maxHealth;
                // called on the Server, but invoked on the Clients
                //Can't just set client position because local client has
                //authority. Need client to run its own position reset via rpc.
                RpcRespawn();
            }
        }
    }
    
    public void OnChangeHealth(int newHealth)
    {
        healthBar.sizeDelta = new Vector2(newHealth, healthBar.sizeDelta.y);   
    }

    [ClientRpc]
    void RpcRespawn()
    {
        if (isLocalPlayer)
        {
            // move back to zero location as a default
            Vector3 spawnPoint = Vector3.zero;
            
            // If there is a spawn point array and the array is not empty, pick one at random
            if (spawnPoints != null && spawnPoints.Length > 0)
            {
                spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position;
            }

            // Set the player’s position to the chosen spawn point
            transform.position = spawnPoint;

        }
    }

}

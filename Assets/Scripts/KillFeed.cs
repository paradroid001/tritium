﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillFeed : MonoBehaviour
{
    [SerializeField]
    GameObject _killFeedItemPrefab;
    public float itemTime = 4.0f;
    // Use this for initialization
	void Start () {
        NetworkGameManager.instance.onPlayerKilledCallback += OnKill;	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    
    public void OnKill(string player, string source)
    {
        Debug.Log(source + "killed " + player);
        GameObject go = (GameObject)Instantiate(_killFeedItemPrefab, this.transform);
        go.GetComponent<KillFeedItem>().Setup(player, source);
        go.transform.SetAsFirstSibling(); //move to the top of the list
        Destroy(go, itemTime); //destroy after a short time
    }
}

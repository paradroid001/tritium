﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class NetworkGameManager : MonoBehaviour 
{
    [SerializeField]
    private GameObject _sceneCamera;

    public MatchSettings matchSettings;
    public static NetworkGameManager instance;
    #region Player tracking
	private const string PLAYER_ID_PREFIX = "Player ";
    private static Dictionary<string, PlayerManager> _players = new Dictionary<string, PlayerManager>(); 

    public delegate void OnPlayerKilledCallback(string player, string source);
    public OnPlayerKilledCallback onPlayerKilledCallback;


    void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("More than one NetworkGameManager in the scene.");
        }
        else
            instance = this;
    }

    public static void RegisterPlayer(string netid, PlayerManager playerComponent)
    {
        string playerid = PLAYER_ID_PREFIX + netid;
        _players.Add(playerid, playerComponent);
        playerComponent.transform.name = playerid;
    }

    public static void UnregisterPlayer(string playerid)
    {
        _players.Remove(playerid);
    }

    public static PlayerManager GetPlayer(string playerid)
    {
        return _players[playerid];
    }
    
    public static PlayerManager[] GetAllPlayers()
    {
        return _players.Values.ToArray();
    }

    public void EnableSceneCamera(bool isactive)
    {
        if (_sceneCamera == null)
            return;
        _sceneCamera.SetActive(isactive);
    }

    void OnGUI()
    {
        return;
        GUILayout.BeginArea(new Rect(200, 200, 200, 500));
        GUILayout.BeginVertical();
        foreach (string playerid in _players.Keys)
        {
            GUILayout.Label(playerid + "  -  " + _players[playerid].transform.name);
        }
        GUILayout.EndVertical();
        GUILayout.EndArea();
    }
    #endregion

}

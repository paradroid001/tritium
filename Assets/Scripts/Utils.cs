﻿using UnityEngine;

public class Utils
{
    public static void SetLayerRecursively(GameObject obj, int newLayer)
    {
        if (obj == null)
            return;
        else
        {
            obj.layer = newLayer;
            foreach (Transform child in obj.transform)
            {
                if (child != null)
                    SetLayerRecursively(child.gameObject, newLayer);
            }
        }
    }
}

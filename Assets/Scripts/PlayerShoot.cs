﻿using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(WeaponManager))]
public class PlayerShoot : NetworkBehaviour 
{
    private PlayerWeapon _currentWeapon;

    private const string _playerTag = "Player";
    private WeaponManager _weaponManager;

    [SerializeField]
    private Camera _cam;


    [SerializeField]
    private LayerMask _mask;

    void Start()
    {
        if (_cam == null)
        {   
            Debug.Log("PlayerShoot: Error - no camera referenced");
            this.enabled = false;
        }
        _weaponManager = GetComponent<WeaponManager>();
    }

    void Update()
    {

        _currentWeapon = _weaponManager.GetCurrentWeapon();
        
        if (PauseMenu.IsOn)
            return;
        
        if (Input.GetButtonDown("Reload") )
        {
            _weaponManager.Reload();
            return;
        }
        
        if (_currentWeapon.fireRate <= 0.0f)
        {
            if (Input.GetButtonDown("Fire1") )
            {
                Shoot();
            }
        }
        else //automatic weapons
        {
            if (Input.GetButtonDown("Fire1"))
            {
                InvokeRepeating("Shoot", 0f, 1f/_currentWeapon.fireRate);
            }
            else if (Input.GetButtonUp("Fire1"))
            {
                CancelInvoke("Shoot");
            }
        }
    }

    [Command] // run on server when a client shoots
    void CmdOnShoot()
    {
        //call a method on all clients that will display a muzzle flash
        RpcDoShootEffect();
    }

    [ClientRpc] //called by server on all clients when the client shoots
    void RpcDoShootEffect()
    {
        // play muzzle flash effect
        _weaponManager.GetCurrentWeaponGraphics().muzzleFlash.Play();
    }

    [Command] //called on server when something is shot, tell all clients
    void CmdOnHit(Vector3 hitpos, Vector3 hitnormal)
    {
        RpcDoHitEffect(hitpos, hitnormal);
    }

    [ClientRpc] //called by server on all clients when a surface hit occurs
    void RpcDoHitEffect(Vector3 hitpos, Vector3 hitnormal)
    {
        GameObject hiteffect = (GameObject)Instantiate(_weaponManager.GetCurrentWeaponGraphics().hitEffectPrefab, hitpos, Quaternion.LookRotation(hitnormal) );
        Destroy(hiteffect, 2f);  // destroy after 2 seconds
    }

       
    [Client] //Only called on client
    private void Shoot()
    {
        if (!isLocalPlayer || _weaponManager.isReloading())
            return;

        if (_currentWeapon.bullets <= 0)
        {
            _weaponManager.Reload();
            return;
        }
        else
        {
            _currentWeapon.bullets -= 1;
        }
        // We are shooting, tell the server.
        CmdOnShoot();

        Debug.Log("Bang");
        RaycastHit hit;
        //Hitscan code
        if (Physics.Raycast(_cam.transform.position, _cam.transform.forward, out hit, _currentWeapon.range, _mask))
        {
            //we hit something
            //Debug.Log("We hit " + hit.collider.name);
            if (hit.collider.tag == _playerTag)
            {
                CmdPlayerIsHit(hit.collider.name, (int)_currentWeapon.damage, transform.name);
            }
            //We hit something, call server OnHit method
            CmdOnHit(hit.point, hit.normal);
        }
        if (_currentWeapon.bullets <= 0)
        {
            _weaponManager.Reload();
            return;
        }

    }

    [Command] //Called on Server
    void CmdPlayerIsHit(string playerid, int damage, string sourceID)
    {
        Debug.Log(playerid + " has been shot");
        PlayerManager player = NetworkGameManager.GetPlayer(playerid);
        player.RpcTakeDamage(damage, sourceID); //take damage on all clients
    }

}

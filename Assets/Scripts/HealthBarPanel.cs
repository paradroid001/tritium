﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBarPanel : MonoBehaviour 
{
    [SerializeField]
    private GameObject _healthBar;
    
    public void CreateHealthBar(PlayerManager player)
    {
        //GameObject go = (GameObject)Instantiate(_healthBarPrefab, this.transform);
        _healthBar.GetComponent<HealthBar>().SetPlayer(player);
    }
}

﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PlayerMotor : MonoBehaviour 
{
    [SerializeField]
    private Camera cam;
    private Vector3 _velocity = Vector3.zero;
    private Vector3 _rotation = Vector3.zero;
    private Vector3 _thrusterForce = Vector3.zero;
    private float _cameraRotationX = 0f;
    private float _currentCameraRotationX = 0f;
    
    [SerializeField]
    private float _cameraRotationLimit = 85f;
    
    private Rigidbody _rb;

	// Use this for initialization
	void Start () 
    {
	    _rb = GetComponent<Rigidbody>();	
	}
	
    //runs every physics update
    void FixedUpdate()
    {
        PerformMovement();
        PerformRotation();
    
    }

    //gets a movement vector
    public void Move(Vector3 vel)
    {
        _velocity = vel;
    }

    public void Rotate(Vector3 rot)
    {
        _rotation = rot;
    }

    //get a rotation vector for the camera
    public void RotateCamera(float rotx)
    {
        _cameraRotationX = rotx;
    }

    //get a thrust vector for our thrusters
    public void ApplyThruster(Vector3 t)
    {
        _thrusterForce = t;
    }

    //perform movement based on velocity
    private void PerformMovement()
    {
        if (_velocity != Vector3.zero)
        {
            //not just a translate - does physics/collision checks too.
            _rb.MovePosition(_rb.position + _velocity * Time.fixedDeltaTime);
        }
        if (_thrusterForce != Vector3.zero)
        {
            //use accelleration ignoring mass using ForceMode.Acceleration;
            _rb.AddForce(_thrusterForce * Time.fixedDeltaTime, ForceMode.Acceleration);
        }
    }

    private void PerformRotation()
    {
        _rb.MoveRotation(_rb.rotation * Quaternion.Euler(_rotation));
        if (cam != null)
        {
            //cam.transform.Rotate(-_cameraRotation);
            //Set our rotation and clamp it
            _currentCameraRotationX -= _cameraRotationX;
            _currentCameraRotationX = Mathf.Clamp(_currentCameraRotationX, 
                    -_cameraRotationLimit, _cameraRotationLimit);
            //Apply rotation to camera transform
            cam.transform.localEulerAngles = new Vector3(_currentCameraRotationX, 0f, 0f);
        }
    }
}

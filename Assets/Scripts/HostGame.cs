﻿using UnityEngine;
using UnityEngine.Networking;

public class HostGame : MonoBehaviour {

    [SerializeField]
    private uint _roomSize = 12;
    private string _roomName;
    private NetworkManager _netman;

    public void Start()
    {
        _netman = NetworkManager.singleton;
        if (_netman.matchMaker == null)
        {
            _netman.StartMatchMaker();
        }
    }

    public void SetRoomName(string name)
    {
        _roomName = name;
    }

    public void SetRoomSize(int size)
    {
        _roomSize = (uint)size;
    }

    public void CreateRoom()
    {
        if (_roomName != null && _roomName != "")
        {
            Debug.Log("Creating room " + _roomName + " with size " + _roomSize);
            //name, size, advertise, password, pub addr, priv addr, eloscore, requestdomain, callback
            _netman.matchMaker.CreateMatch(_roomName, _roomSize, true, "", "", "", 0, 0,  _netman.OnMatchCreate);
        }
    }
}

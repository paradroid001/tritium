﻿using UnityEngine;
using UnityEngine.Networking;
using System.Collections;

public class WeaponManager : NetworkBehaviour 
{
    [SerializeField]
    private string _weaponLayerName = "Weapon";
    [SerializeField]
    private PlayerWeapon _primaryWeapon;
    private PlayerWeapon _currentWeapon;
    [SerializeField]
    private Transform _weaponHolder;
    private WeaponGraphics _currentGraphics;
    private bool _isReloading;

    public bool isReloading()
    {
        return _isReloading;
    }

	// Use this for initialization
	void Start () 
    {
	    EquipWeapon(_primaryWeapon);
	}

    void EquipWeapon(PlayerWeapon weapon)
    {
        _currentWeapon = weapon;
        GameObject weaponInst = (GameObject)Instantiate(weapon.graphics, _weaponHolder.position, weapon.graphics.transform.rotation);
        _currentGraphics = weaponInst.GetComponent<WeaponGraphics>();
        if (_currentGraphics == null)
        {
            Debug.LogError("No weapon graphics on " + weaponInst.name);
        }
        weaponInst.transform.SetParent(_weaponHolder);
        
        if (isLocalPlayer)
        {    
            Utils.SetLayerRecursively(weaponInst, LayerMask.NameToLayer(_weaponLayerName));
        }
    }
    public PlayerWeapon GetCurrentWeapon()
    {
        return _currentWeapon;
    }
    public WeaponGraphics GetCurrentWeaponGraphics()
    {
        return _currentGraphics;
    }

    
    public void Reload()
    {
        //Don't reload if you're already reloading or if you're full
        if (_isReloading || _currentWeapon.bullets == _currentWeapon.maxBullets)
            return;
        StartCoroutine(Reload_Coroutine() );
    }

    private IEnumerator Reload_Coroutine()
    {
        Debug.Log("Reloading");
        _isReloading = true;
        CmdOnReload();
        yield return new WaitForSeconds(_currentWeapon.reloadTime);
        _currentWeapon.bullets = _currentWeapon.maxBullets;
        _isReloading = false;

    }

    [Command]
    void CmdOnReload()
    {
        RpcOnReload();
    }

    [ClientRpc]
    void RpcOnReload()
    {
        Animator anim = _currentGraphics.GetComponent<Animator>();
        if (anim != null)
        {
            anim.SetTrigger("Reload");
        }
    }
}

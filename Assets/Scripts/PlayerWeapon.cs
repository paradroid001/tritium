﻿using UnityEngine;

[System.Serializable] //so we can save instances
public class PlayerWeapon 
{
    public string name = "Glock";
    public float damage = 10f;
    public float range = 100f;
    public int maxBullets = 20;
    [HideInInspector]
    public int bullets;
    public GameObject graphics;
    public float fireRate = 0f; //0 is non auto, anything above is automatic
    public float reloadTime = 2.0f;
    public PlayerWeapon()
    {
        bullets = maxBullets;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour 
{
    public RectTransform _healthBarFill;
    private PlayerManager _player;

    public void SetPlayer(PlayerManager player)
    {
        _player = player;
    }
	
	// Update is called once per frame
	void Update () 
    {
	    if (_player != null && _healthBarFill != null)
        {
            SetHealthAmount(_player.GetHealthPercentage() );    
        }
	}

    void SetHealthAmount(float amount)
    {
        _healthBarFill.localScale = new Vector3(amount, 1f, 1f);
    }

}

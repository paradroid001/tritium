﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class PauseMenu : MonoBehaviour 
{
    public static bool IsOn = false;
    private NetworkManager _netman;
	// Use this for initialization
	void Start () 
    {
		_netman = NetworkManager.singleton;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void LeaveRoom()
    {
        MatchInfo matchInfo = _netman.matchInfo;
        _netman.matchMaker.DropConnection(matchInfo.networkId, matchInfo.nodeId, 0, _netman.OnDropConnection);
        _netman.StopHost(); //even if we aren't the host.
    }
}

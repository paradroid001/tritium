﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerNamePlate : MonoBehaviour 
{
    [SerializeField]
    private Text _usernameText;
    [SerializeField]
    private PlayerManager _player;
    [SerializeField]
    private HealthBar _healthBar;

    public void SetPlayer(PlayerManager player)
    {
        _player = player;
        _healthBar.SetPlayer(_player);
    }

	// Update is called once per frame
	void Update () 
    {
		_usernameText.text = _player.username;
	}
}

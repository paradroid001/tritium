﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;
using UnityEngine.Networking.Match;

public class JoinGame : MonoBehaviour {

    private NetworkManager _netman;
    List<GameObject> _roomList = new List<GameObject>();
    [SerializeField]
    private Text _statusText;
    [SerializeField]
    private GameObject _roomListItemPrefab;
    [SerializeField]
    private Transform _roomListScrollView;

	// Use this for initialization
	void Start () 
    {
	    _netman = NetworkManager.singleton;
        if (_netman.matchMaker == null)
        {
            _netman.StartMatchMaker();
        }

        //RefreshRoomList();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    //called by refresh button
    public void RefreshRoomList()
    {
        //Clear current room list.
        ClearRoomList();
        if (_netman.matchMaker == null)
        {
            _netman.StartMatchMaker();
        }

        //get first 20, filter, callback,
        _netman.matchMaker.ListMatches(0, 20, "", false, 0, 0,  OnMatchList);
        _statusText.text = "Loading...";
    }

    public void OnMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matchList)
    {
        _statusText.text = "";
        if (!success || matchList == null)
        {
            _statusText.text = "Couldn't get matches";
            return;
        }

        if (matchList.Count == 0)
            _statusText.text = "No rooms at the moment.";
        foreach (MatchInfoSnapshot match in matchList)
        {
            GameObject listitem = Instantiate(_roomListItemPrefab);
            listitem.transform.SetParent(_roomListScrollView);
            //have a component sitting on the GO that will take care
            //of setting up user text and the call back which joins
            //the game.
            RoomListItem rli = listitem.GetComponent<RoomListItem>();
            if (rli != null)
            {
                rli.Setup(match, JoinRoom);
            }
            _roomList.Add(listitem);
        }
    }

    private void ClearRoomList()
    {
        for (int i = 0; i < _roomList.Count; i++)
        {
            Destroy(_roomList[i]);
        }
        _roomList.Clear();
    }

    public void JoinRoom(MatchInfoSnapshot match)
    {
        Debug.Log("Join Match " + match.name);
        _netman.matchMaker.JoinMatch(match.networkId, "", "", "", 0, 0, _netman.OnMatchJoined);
        StartCoroutine(WaitForJoin());
    }

    IEnumerator WaitForJoin()
    {
        ClearRoomList();
        _statusText.text = "Joining...";
        int countdown = 10;
        while (countdown > 0)
        {
            _statusText.text = "Joining...(" + countdown + ")";
            yield return new WaitForSeconds(1.0f);
            countdown --;
        }
        _statusText.text = "Failed to connect";
        yield return new WaitForSeconds(1.0f);
        MatchInfo matchInfo = _netman.matchInfo;
        if (matchInfo != null)
        {
            _netman.matchMaker.DropConnection(matchInfo.networkId, matchInfo.nodeId, 0, _netman.OnDropConnection);
            _netman.StopHost(); //even if we aren't the host.
        }
        RefreshRoomList();
    }

}

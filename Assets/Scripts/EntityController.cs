﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ConfigurableJoint))]
[RequireComponent(typeof(PlayerMotor))]
[RequireComponent(typeof(Animator))]
public class EntityController : MonoBehaviour 
{
    [SerializeField]
    private float _speed = 5f;
    [SerializeField]
    private float _lookSensitivity = 3f;
    [SerializeField]
    private float thrusterForce = 1000f;
    [SerializeField]
    private float _thrusterBurnSpeed = 1f; //burnt in 1 second
    [SerializeField]
    private float _thrusterRegenSpeed = 0.3f; 
    private float _thrusterFuelAmount = 1f;

    public float GetThrusterFuelAmount()
    {
        return _thrusterFuelAmount;
    }

    [SerializeField]
    private LayerMask _environmentMask;
    
    [Header("Spring Settings")]
    [SerializeField]
    private float _jointSpring = 20.0f;
    [SerializeField]
    private float _jointMaxForce = 40.0f;

    private PlayerMotor motor;
    private ConfigurableJoint joint;
    private Animator animator;

    void Start()
    {
        motor = GetComponent<PlayerMotor>();
        joint = GetComponent<ConfigurableJoint>();
        SetJointSettings(_jointSpring);
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (PauseMenu.IsOn)
        {
            if (Cursor.lockState != CursorLockMode.None)
            {
                Cursor.lockState = CursorLockMode.None;
            }
            motor.Move(Vector3.zero);
            motor.Rotate(Vector3.zero);
            motor.RotateCamera(0f);
            return;
        }
        if (Cursor.lockState != CursorLockMode.Locked)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        //Detect groundplane or platform below us so we can set target for floating.
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, 100f, _environmentMask) )
        {
            joint.targetPosition = new Vector3(0f, -hit.point.y, 0f);
        }
        else
        {
            joint.targetPosition = new Vector3(0f, 0f, 0f);
        }
        //calculate movement velocity as a 3d vector
        //float _xmove = Input.GetAxisRaw("Horizontal"); //sideways
        //float _zmove = Input.GetAxisRaw("Vertical"); //forward and back
        float _xmove = Input.GetAxis("Horizontal"); //sideways
        float _zmove = Input.GetAxis("Vertical"); //forward and back
        Vector3 _moveH = transform.right * _xmove; //(1, 0, 0) * movement
        Vector3 _moveV = transform.forward * _zmove; //(0, 0, 1) * movement
        //Final movement vector
        //Vector3 _vel = (_moveH + _moveV).normalized * _speed;
        Vector3 _vel = (_moveH + _moveV) * _speed;
        
        //Animate movement
        animator.SetFloat("ForwardVelocity", _zmove);
        
        //Apply movement
        motor.Move(_vel);

        //calculate rot as a 3d vector
        float _yrot = Input.GetAxisRaw("Mouse X"); //side to side = yaw
        float _xrot = Input.GetAxisRaw("Mouse Y"); //up and down = pitch
        Vector3 _playerrot = new Vector3(0f, _yrot, 0f) * _lookSensitivity; //turn the player
        //Vector3 _camrot = new Vector3(_xrot, 0f, 0f) * _lookSensitivity; //turn the cam
        float _camrotX = _xrot * _lookSensitivity;

        //Apply player rotation
        motor.Rotate(_playerrot);
        //Apply cam rotation
        motor.RotateCamera(_camrotX);
        
        //Calculate thruster force based on player input
        Vector3 tForce = Vector3.zero;
        if (Input.GetButton("Jump") && _thrusterFuelAmount > 0.0f)
        {
            //use fuel
            _thrusterFuelAmount -= _thrusterBurnSpeed * Time.deltaTime;

            //need to regen a little bit before using again
            if (_thrusterFuelAmount >= 0.01f) 
            {
                tForce = Vector3.up * thrusterForce;
                SetJointSettings(0f);
            }
        }
        else
        {
            _thrusterFuelAmount += _thrusterRegenSpeed * Time.deltaTime;
            SetJointSettings(_jointSpring);
        }
        
        //Either way clamp the fuel between 0 and 1
        _thrusterFuelAmount = Mathf.Clamp(_thrusterFuelAmount, 0.0f, 1.0f);

        //Apply thruster force
        motor.ApplyThruster(tForce);
    }

    private void SetJointSettings(float jointSpring)
    {
        joint.yDrive = new JointDrive{ 
            positionSpring = jointSpring, 
            maximumForce = _jointMaxForce};
    }
}
